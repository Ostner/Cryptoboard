//
//  AppDelegate.swift
//  Cryptoboard
//
//  Created by Tobias Ostner on 01.11.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import CryptoComparePlatform

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?)
      -> Bool
    {
        window = UIWindow()

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! UINavigationController
        let useCaseProvider = UseCaseProvider()
        let navigator = CoinListNavigator(
          services: useCaseProvider,
          navigationController: navigationController,
          storyboard: storyboard)
        navigator.toCoinList()
        window?.rootViewController = navigationController

        window?.makeKeyAndVisible()
        return true
    }

}
