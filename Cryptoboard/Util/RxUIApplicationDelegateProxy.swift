//
//  RxUIApplicationDelegateProxy.swift
//  Cryptoboard
//
//  Created by Tobias Ostner on 08.11.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import RxSwift
import RxCocoa

class RxApplicationDelegateProxy:
  DelegateProxy<UIApplication, UIApplicationDelegate>,
  DelegateProxyType,
  UIApplicationDelegate
{

    private(set) var application: UIApplication?

    init(application: ParentObject) {
        self.application = application
        super.init(parentObject: application, delegateProxy: RxApplicationDelegateProxy.self)
    }

    static func registerKnownImplementations() {
        self.register { RxApplicationDelegateProxy(application: $0) }
    }

    static func currentDelegate(for object: UIApplication) -> UIApplicationDelegate? {
        return object.delegate
    }

    static func setCurrentDelegate(_ delegate: UIApplicationDelegate?, to object: UIApplication) {
        object.delegate = delegate
    }

    override func setForwardToDelegate(_ forwardToDelegate: UIApplicationDelegate?, retainDelegate: Bool) {
        super.setForwardToDelegate(forwardToDelegate, retainDelegate: true)
    }
}

extension Reactive where Base: UIApplication {

    var delegate: DelegateProxy<UIApplication, UIApplicationDelegate> {
        return RxApplicationDelegateProxy.proxy(for: base)
    }

    var didBecomeActive: Observable<Void> {
        return delegate.methodInvoked(#selector(UIApplicationDelegate.applicationDidBecomeActive(_:)))
          .map { _ in () }
    }
}
