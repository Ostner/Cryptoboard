//
//  UIColor+.swift
//  Cryptoboard
//
//  Created by Tobias Ostner on 15.11.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

extension UIColor {
    static let cream = UIColor(named: "Cream")!
    static let ufoGreen = UIColor(named: "UfoGreen")!
    static let moderateRed = UIColor(named: "ModerateRed")!
    static let textGrey = UIColor(named: "TextGrey")!
    static let travertine = UIColor(named: "Travertine")!
}
