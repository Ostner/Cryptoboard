//
//  NSAttributedString+.swift
//  Cryptoboard
//
//  Created by Tobias Ostner on 29.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation

extension NSAttributedString {
    static func concat(_ strings: NSAttributedString? ...) -> NSAttributedString {
        return strings.reduce(into: NSMutableAttributedString()) { result, part in
            guard let part = part else { return }
            result.append(part)
        }
    }

    static func +(lhs: NSAttributedString, rhs: NSAttributedString?) -> NSAttributedString {
        return NSAttributedString.concat(lhs, rhs)
    }
}
