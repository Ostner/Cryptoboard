//
//  CoinInfo+.swift
//  Cryptoboard
//
//  Created by Tobias Ostner on 01.02.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain
import RxDataSources

extension CoinInfo: IdentifiableType {
    public var identity: Int {
        return hashValue
    }
}
