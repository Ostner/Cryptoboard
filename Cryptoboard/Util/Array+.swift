//
//  Extensions.swift
//  Cryptoboard
//
//  Created by Tobias Ostner on 29.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation

extension Array {
    mutating func replace(_ element: Element, where predicate: (Element) -> Bool) {
        guard let index = index(where: predicate) else { return }
        let range = index ..< index + 1
        self.replaceSubrange(range, with: [element])
    }
}
