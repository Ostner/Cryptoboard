//
//  BindingExtensions.swift
//  Cryptoboard
//
//  Created by Tobias Ostner on 14.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Charts

extension Reactive where Base: UILabel {
    var textColor: Binder<UIColor> {
        return Binder(base) { label, color in
            label.textColor = color
        }
    }
}

extension Reactive where Base: UIView {
    var accessibilityLabel: Binder<String?> {
        return Binder(base) { view, label in
            view.accessibilityLabel = label
        }
    }
}

extension Reactive where Base: LineChartView {
    var data: Binder<ChartData> {
        return Binder(base) { chart, data in
            chart.data = data
        }

    }
}
