//
//  UseCaseProvider.swift
//  Cryptoboard
//
//  Created by Tobias Ostner on 18.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import CryptoComparePlatform
import CoinMarketCapPlatform
import CoinCapPlatform
import Domain

final class UseCaseProvider: Domain.UseCaseProvider {

    let cryptoCompareProvider = CryptoComparePlatform.UseCaseProvider()
    let coinMarketCapProvider = CoinMarketCapPlatform.UseCaseProvider()
    let coinCapProvider = CoinCapPlatform.UseCaseProvider()

    var coinInfosUseCase: Domain.CoinInfosUseCase {
        return coinCapProvider.coinInfosUseCase
    }

    var coinPricesUseCase: Domain.CoinPricesUseCase {
        return cryptoCompareProvider.coinPricesUseCase
    }

}
