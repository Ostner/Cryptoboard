//
//  CoinDetailViewModel.swift
//  Cryptoboard
//
//  Created by Tobias Ostner on 15.11.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import Domain
import RxSwift
import RxCocoa

final class CoinDetailViewModel: ViewModel {

    struct Output {
        let coinName: Driver<String?>
        let shortDescription: Driver<String?>
        let abbreviation: Driver<String>
        let prices: Driver<CoinPrices>
    }

    private let coinInfo: CoinInfo
    private let useCase: CoinPricesUseCase

    init(useCase: CoinPricesUseCase, coinInfo: CoinInfo) {
        self.coinInfo = coinInfo
        self.useCase = useCase
    }

    func transform(input: Void) -> Output {
        let prices = useCase
          .fetchDays(coin: coinInfo.coin, in: coinInfo.currency)
          .asDriver(onErrorJustReturn: CoinPrices(coin: coinInfo.coin, prices: []))
        return Output(
          coinName: Driver.just(coinInfo.coin.name),
          shortDescription: Driver.just(shortDescription[coinInfo.coin.name]),
          abbreviation: Driver.just(coinInfo.coin.abbreviation),
          prices: prices)

    }
}

