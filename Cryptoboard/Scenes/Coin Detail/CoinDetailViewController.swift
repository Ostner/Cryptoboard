//
//  CoinDetailViewController.swift
//  Cryptoboard
//
//  Created by Tobias Ostner on 13.11.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Charts

final class CoinDetailViewController: UIViewController {

    class DayAxisFormatter: NSObject, IAxisValueFormatter {
        func stringForValue(_ value: Double, axis: AxisBase?) -> String {
            let date = Date(timeIntervalSince1970: value)
            let components = Calendar.current.dateComponents([.day, .month], from: date)
            return "\(components.day!)"
        }
    }

    @IBOutlet weak var coinName: UILabel!
    @IBOutlet weak var shortDescription: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var priceGraph: LineChartView! {
        didSet {
            priceGraph.chartDescription?.enabled = false
            priceGraph.legend.enabled = false
            priceGraph.dragEnabled = false
            priceGraph.pinchZoomEnabled = false
            priceGraph.borderColor = .cream
            priceGraph.drawBordersEnabled = true
            priceGraph.xAxis.valueFormatter = DayAxisFormatter()
            priceGraph.xAxis.drawGridLinesEnabled = true
            priceGraph.xAxis.labelTextColor = .cream
            priceGraph.xAxis.gridColor = .cream
            priceGraph.xAxis.labelPosition = .bottom
            priceGraph.xAxis.labelFont = .preferredFont(forTextStyle: .footnote)
            priceGraph.leftAxis.labelTextColor = .cream
            priceGraph.leftAxis.gridColor = .cream
            priceGraph.leftAxis.labelPosition = .outsideChart
            priceGraph.leftAxis.labelFont = .preferredFont(forTextStyle: .footnote)
            priceGraph.rightAxis.enabled = false
        }
    }

    var viewModel: CoinDetailViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
    }

    private let disposeBag = DisposeBag()

    private let pricesData: LineChartDataSet = {
        let dataSet = LineChartDataSet(values: [], label: nil)
        dataSet.drawCirclesEnabled = true
        dataSet.circleRadius = 3
        dataSet.drawValuesEnabled = false
        dataSet.drawFilledEnabled = true
        dataSet.setColor(.cream)
        dataSet.fillColor = .cream
        dataSet.circleColors = [.cream]
        dataSet.lineWidth = 2
        return dataSet
    }()

    private func bindViewModel() {
        let output = viewModel.transform(input: ())
        output.coinName.drive(coinName.rx.text).disposed(by: disposeBag)
        output.coinName.drive(coinName.rx.accessibilityLabel).disposed(by: disposeBag)
        output.shortDescription.drive(shortDescription.rx.text).disposed(by: disposeBag)
        output.shortDescription.drive(shortDescription.rx.accessibilityLabel).disposed(by: disposeBag)
        output
          .abbreviation
          .map { UIImage(named: "\($0).pdf") }
          .drive(icon.rx.image)
          .disposed(by: disposeBag)
        output
          .prices
          .map { coinPrices in
              return coinPrices.prices
                .map { ChartDataEntry(x: $0.date.timeIntervalSince1970, y: $0.close) }
          }
          .map { [weak self] entries in
              self?.pricesData.values = entries
              return LineChartData(dataSet: self?.pricesData)
          }
          .drive(priceGraph.rx.data)
          .disposed(by: disposeBag)
    }

}

