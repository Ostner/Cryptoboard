//
//  CoinInfoSection.swift
//  Cryptoboard
//
//  Created by Tobias Ostner on 01.02.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain
import RxDataSources

struct CoinInfoSection {
    var items: [Item]
}

extension CoinInfoSection: AnimatableSectionModelType {
    
    typealias Item = CoinInfo
    
    var identity: String {
        return "no identity"
    }
    
    init(original: CoinInfoSection, items: [Item]) {
        self = original
        self.items = items
    }
}
