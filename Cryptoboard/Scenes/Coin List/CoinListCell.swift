//
//  CoinListCell.swift
//  Cryptoboard
//
//  Created by Tobias Ostner on 12.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import Domain
import RxSwift
import RxCocoa

final class CoinListCell: UICollectionViewCell {

    private var viewModel: CoinListItemViewModel!

    @IBOutlet weak var coinName: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var relativeChange: UILabel!

    let fontSize: CGFloat = 45.0

    var font: UIFont {
        return .systemFont(ofSize: fontSize)
    }

    var currencyFont: UIFont {
        let size = CGFloat(2.0/3.0) * fontSize
        return .systemFont(ofSize: size)
    }

    private let _currencySymbol = "$"

    func configure(_ viewModel: CoinListItemViewModel) {
        self.viewModel = viewModel
        let output = viewModel.transform(input: ())
        coinName.text = output.coinName
        coinName.accessibilityLabel = output.coinName
        price.attributedText = currencySymbol + attributedPrice(output.price)
        relativeChange.text = output.relativeChange
        let textColor: UIColor = output.isPositive ? .ufoGreen : .moderateRed
        relativeChange.textColor = textColor
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        viewModel = nil
    }

    private var currencySymbol: NSAttributedString {
        return NSAttributedString(
          string: _currencySymbol,
          attributes: [.font: currencyFont, .foregroundColor: UIColor.travertine]
        )
    }

    private func attributedPrice(_ string: String?) -> NSAttributedString? {
        guard let string = string else { return nil }
        return NSAttributedString(
          string: string,
          attributes: [.font: font, .foregroundColor: UIColor.cream]
        )
    }

}

