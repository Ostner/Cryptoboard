//
//  CoinListItemViewModel.swift
//  Cryptoboard
//
//  Created by Tobias Ostner on 14.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain
import RxSwift
import RxCocoa

final class CoinListItemViewModel: ViewModel {

    private let coinInfo: CoinInfo

    init(_ coinInfo: CoinInfo) {
        self.coinInfo = coinInfo
    }

    struct Output {
        let coinName: String?
        let price: String?
        let nominalChange: String?
        let relativeChange: String?
        let isPositive: Bool
    }

    func transform(input: ()) -> Output {
        let coinName = coinInfo.coin.name
        let price = priceString
        let nominalChange = nominalChangeString
        let relativeChange = relativeChangeString
        let isPositive = coinInfo.nominalDayChange >= 0
        return Output(
          coinName: coinName,
          price: price,
          nominalChange: nominalChange,
          relativeChange: relativeChange,
          isPositive: isPositive)
    }

    private var nominalValueFormatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        switch coinInfo.price {
        case ..<1:
            formatter.minimumFractionDigits = 4
            formatter.maximumFractionDigits = 4
        case 1..<1000:
            formatter.minimumFractionDigits = 2
            formatter.maximumFractionDigits = 2
        case 1000...:
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 0
        default: break
        }
        return formatter
    }

    private var nominalChangeFormatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        switch abs(coinInfo.nominalDayChange) {
        case ..<1:
            formatter.minimumFractionDigits = 4
            formatter.maximumFractionDigits = 4
        case 1..<1000:
            formatter.minimumFractionDigits = 2
            formatter.maximumFractionDigits = 2
        case 1000...:
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 0
        default: break
        }
        return formatter
    }

    private var relativeChangeFormatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.multiplier = 1
        formatter.positivePrefix = "+"
        formatter.negativePrefix = "-"
        return formatter
    }

    private var priceString: String? {
        return nominalValueFormatter.string(for: coinInfo.price)
    }


    private var nominalChangeString: String? {
        guard let valueString = nominalChangeFormatter.string(for: coinInfo.nominalDayChange) else { return nil }
        return coinInfo.nominalDayChange < 0 ? valueString : "+" + valueString
    }


    private var relativeChangeString: String? {
        return relativeChangeFormatter.string(for: coinInfo.relativeDayChange)
    }

}
