//
//  MainViewModel.swift
//  Cryptoboard
//
//  Created by Tobias Ostner on 01.11.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya
import Domain

final class CoinListViewModel {

    struct Input {
        let trigger: Driver<Void>
        let selection: Driver<IndexPath>
    }

    struct Output {
        let sections: Driver<[CoinInfoSection]>
        let refreshing: Driver<Bool>
        let selectedCoin: Driver<CoinInfo>
    }

    private let useCase: CoinInfosUseCase
    private let navigator: CoinListNavigating
    private let bag = DisposeBag()

    init(useCase: CoinInfosUseCase, navigator: CoinListNavigating) {
        self.useCase = useCase
        self.navigator = navigator
    }

    func transform(input: Input) -> Output {
        let coinInfos = input.trigger
          .debug("trigger")
          .flatMapLatest {
              return self.useCase.fetch(in: .usd)
                .asDriver(onErrorJustReturn: [])
          }

        let refreshing = Observable
          .from([input.trigger.map { _ in true }.asObservable(),
                 coinInfos.map { _ in false }.asObservable()])
          .merge()
          .debug("refreshing")
          .asDriver(onErrorJustReturn: false)

        let selectedCoin = input.selection
          .withLatestFrom(coinInfos) { indexPath, coinInfos -> CoinInfo in
              return coinInfos[indexPath.row]
          }
          .do(onNext: navigator.toCoinDetail)

        let connection = useCase.connect(in: .usd)
          .debug("connection")
          .skipUntil(coinInfos.asObservable())
          .asDriver(onErrorJustReturn: [])

        let s = Observable.of(coinInfos, connection)
          .merge()
          .scan([]) { old, new in
              guard !old.isEmpty else { return new }
              var updated = old
              new.forEach { coinInfo in
                  updated.replace(coinInfo) {
                      $0.coin.abbreviation == coinInfo.coin.abbreviation
                  }
              }
              return updated
          }
          .map { [CoinInfoSection(items: $0)] }
          .asDriver(onErrorJustReturn: [])

        return Output(
          sections: s,
          refreshing: refreshing,
          selectedCoin: selectedCoin)
    }

}
