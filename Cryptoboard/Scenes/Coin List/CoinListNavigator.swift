//
//  CoinListNavigator.swift
//  Cryptoboard
//
//  Created by Tobias Ostner on 12.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import Domain

public protocol CoinListNavigating {
    func toCoinList()
    func toCoinDetail(coinInfo: CoinInfo)
}

class CoinListNavigator: CoinListNavigating {

    private let storyboard: UIStoryboard

    private let navigationController: UINavigationController

    private let services: Domain.UseCaseProvider

    init(services: Domain.UseCaseProvider,
         navigationController: UINavigationController,
         storyboard: UIStoryboard)
    {
        self.services = services
        self.navigationController = navigationController
        self.storyboard = storyboard
    }

    func toCoinList() {
        let vc = storyboard.instantiateViewController(withIdentifier: "CoinList") as! CoinListViewController
        vc.viewModel = CoinListViewModel(
          useCase: services.coinInfosUseCase,
          navigator: self)
        navigationController.pushViewController(vc, animated: true)
    }

    func toCoinDetail(coinInfo: CoinInfo) {
        let vc = storyboard.instantiateViewController(withIdentifier: "CoinDetail") as! CoinDetailViewController
        vc.viewModel = CoinDetailViewModel(
          useCase: services.coinPricesUseCase,
          coinInfo: coinInfo)
        navigationController.pushViewController(vc, animated: true)
    }
}
