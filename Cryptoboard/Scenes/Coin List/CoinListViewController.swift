//
//  ViewController.swift
//  Cryptoboard
//
//  Created by Tobias Ostner on 01.11.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Moya
import RxDataSources
import Domain

final class CoinListViewController: UIViewController {

    private let disposeBag = DisposeBag()

    private var refresher: UIRefreshControl!

    private let horizontalInsetFraction: CGFloat = 0.05

    @IBOutlet weak var collectionView: UICollectionView!

    var viewModel: CoinListViewModel!

    var dataSource: RxCollectionViewSectionedAnimatedDataSource<CoinInfoSection>!

    override func viewDidLoad() {
        super.viewDidLoad()

        refresher = UIRefreshControl()
        refresher.tintColor = .cream
        collectionView.refreshControl = refresher

        dataSource = RxCollectionViewSectionedAnimatedDataSource(
          configureCell: configureCell,
          configureSupplementaryView: { _, _, _, _ in UICollectionReusableView() }
        )

        bindViewModel()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let width = collectionView.bounds.width
        layout.sectionInset.left = width * horizontalInsetFraction
        layout.sectionInset.right = width * horizontalInsetFraction
        layout.itemSize.height = collectionView.bounds.height * 0.15
        layout.itemSize.width = collectionView.bounds.width - layout.sectionInset.left - layout.sectionInset.right
    }

    private func bindViewModel() {
        let becomesActive = UIApplication.shared.rx.didBecomeActive.asDriver(onErrorJustReturn: ())
        let pullToRefresh = refresher.rx.controlEvent(.valueChanged).asDriver()
        let selection = collectionView.rx.itemSelected.asDriver()
        let input = CoinListViewModel.Input(
          trigger: Driver.merge(becomesActive, pullToRefresh),
          selection: selection)

        let output = viewModel.transform(input: input)

        output.sections
          .drive(collectionView.rx.items(dataSource: dataSource))
          .disposed(by: disposeBag)

        output.refreshing
          .drive(refresher.rx.isRefreshing)
          .disposed(by: disposeBag)

        output.selectedCoin
          .drive()
          .disposed(by: disposeBag)
    }

    private func configureCell(
      _ dataSource: CollectionViewSectionedDataSource<CoinInfoSection>,
      _ collectionView: UICollectionView,
      _ indexPath: IndexPath,
      _ item: CoinInfo
    ) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
          withReuseIdentifier: "CoinListCell",
          for: indexPath) as! CoinListCell
        let viewModel = CoinListItemViewModel(item)
        cell.configure(viewModel)
        return cell
    }

}
