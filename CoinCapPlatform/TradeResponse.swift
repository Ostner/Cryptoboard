//
//  TradeResponse.swift
//  CoinCapPlatform
//
//  Created by Tobias Ostner on 22.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation

struct TradeResponse: Decodable {

    let exchange: String
    let relativeChange: Double
    let price: Double
    let coinName: String
    let coinAbbrev: String
    let marketCap: Double

    enum CodingKeys: String, CodingKey {
        case exchange = "exchange_id"
        case message = "msg"
    }

    enum Message: String, CodingKey {
        case relativeChange = "perc"
        case price
        case coinName = "long"
        case coinAbbrev = "short"
        case marketCap = "mktcap"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.exchange = try container.decode(String.self, forKey: .exchange)
        let msg = try container.nestedContainer(keyedBy: Message.self, forKey: .message)
        self.relativeChange = try msg.decode(Double.self, forKey: .relativeChange)
        self.price = try msg.decode(Double.self, forKey: .price)
        self.coinName = try msg.decode(String.self, forKey: .coinName)
        self.coinAbbrev = try msg.decode(String.self, forKey: .coinAbbrev)
        self.marketCap = try msg.decode(Double.self, forKey: .marketCap)
    }
}
