//
//  CoinCapAPI.swift
//  CoinCapPlatform
//
//  Created by Tobias Ostner on 21.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation
import Moya

enum CoinCapAPI {
    case front
}

extension CoinCapAPI: TargetType {

    var baseURL: URL {
        return URL(string: "https://coincap.io")!
    }

    var path: String {
        return "/front"
    }

    var method: Moya.Method {
        return .get
    }

    var task: Task {
        return Task.requestPlain
    }

    var headers: [String: String]? {
        return nil
    }

    var sampleData: Data {
        return
          """
          [
            {
             "cap24hrChange": -6.05,
             "long": "Bitcoin",
             "mktcap": 65173805891.25,
             "perc": -6.05,
             "price": 3934.85,
             "shapeshift": true,
             "short": "BTC",
             "supply": 16563225,
             "usdVolume": 2337600000,
             "volume": 2337600000,
             "vwapData": 3997.5639538606733,
             "vwapDataBTC": 3997.5639538606733
            },
            {
             "cap24hrChange": -6.59,
             "long": "Ethereum",
             "mktcap": 26016428866.32,
             "perc": -6.59,
             "price": 275.02,
             "shapeshift": true,
             "short": "ETH",
             "supply": 94598316,
             "usdVolume": 945732000,
             "volume": 945732000,
             "vwapData": 278.03921067242516,
             "vwapDataBTC": 278.03921067242516
            }
          ]
          """.data(using: .utf8)!
    }
}
