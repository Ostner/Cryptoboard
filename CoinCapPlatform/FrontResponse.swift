//
//  FrontResponse.swift
//  CoinCapPlatform
//
//  Created by Tobias Ostner on 21.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation

struct FrontResponseElement: Decodable {
    let coinName: String
    let coinAbbrev: String
    let price: Double
    let marketCap: Double
    let relativeChange: Double

    enum CodingKeys: String, CodingKey {
        case coinName = "long"
        case coinAbbrev = "short"
        case price
        case marketCap = "mktcap"
        case relativeChange = "perc"
    }
}
