//
//  CoinCapPlatform.h
//  CoinCapPlatform
//
//  Created by Tobias Ostner on 21.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CoinCapPlatform.
FOUNDATION_EXPORT double CoinCapPlatformVersionNumber;

//! Project version string for CoinCapPlatform.
FOUNDATION_EXPORT const unsigned char CoinCapPlatformVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoinCapPlatform/PublicHeader.h>


