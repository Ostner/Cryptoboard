//
//  UseCaseProvider.swift
//  CoinCapPlatform
//
//  Created by Tobias Ostner on 21.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain
import Moya

public final class UseCaseProvider: Domain.UseCaseProvider {

    private let _coinInfosUseCase = CoinInfosUseCase()

    public init() {}

    public var coinInfosUseCase: Domain.CoinInfosUseCase {
        return _coinInfosUseCase
    }

    public var coinPricesUseCase: Domain.CoinPricesUseCase {
        fatalError("not implemented yet")
    }
}
