//
//  CoinCapSocketManager.swift
//  CoinCapPlatform
//
//  Created by Tobias Ostner on 22.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation
import SocketIO
import RxSwift
import Domain

final public class CoinCapSocketManager {

    public enum Event: String {
        case trade = "trades"
    }

    private let url = URL(string: "https://coincap.io")!
    private let socketManager: SocketManager

    public let socket: SocketIOClient

    init() {
        socketManager = SocketManager(socketURL: url, config: [.log(false)])
        socket = socketManager.defaultSocket
    }

    public func connect() {
        guard !socketManager.status.active else { return }
        socketManager.connect()
    }

    public func close() {
        socketManager.disconnect()
    }

    public var trades: Observable<[Any]> {
        return Observable.create { observer in
            let id = self.socket.on("trades") { data, _ in
                observer.onNext(data)
            }
            return Disposables.create {
                self.socket.off(id: id)
            }
        }
    }

}
