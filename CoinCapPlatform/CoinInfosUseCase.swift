//
//  CoinInfosUseCase.swift
//  CoinCapPlatform
//
//  Created by Tobias Ostner on 21.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain
import Moya
import RxSwift

final class CoinInfosUseCase: Domain.CoinInfosUseCase  {

    let httpProvider: MoyaProvider<CoinCapAPI>
    let socketProvider = CoinCapSocketManager()

    init() {
        #if TEST
        httpProvider = MoyaProvider<CoinCapAPI>(stubClosure: MoyaProvider.immediatelyStub)
        #else
        httpProvider = MoyaProvider<CoinCapAPI>(plugins: [NetworkLoggerPlugin()])
        #endif
    }

    func fetch(coins: [Coin], in currency: Currency) -> Observable<[CoinInfo]> {
        fatalError("not supported by API")
    }

    func fetch(in _: Currency) -> Observable<[CoinInfo]> {
        return httpProvider
          .rx.request(.front)
          .filterSuccessfulStatusCodes()
          .map([FrontResponseElement].self)
          .map { $0.map(CoinInfo.init) }
          .asObservable()
    }

    func connect(in currency: Currency) -> Observable<[CoinInfo]> {

        socketProvider.connect()
        let trades = socketProvider.trades
          .map(tradesResponseToCoinInfos)

        let ticks = Observable<Int>
          .timer(0, period: 60, scheduler: MainScheduler.instance)
          .map { _ in () }

        let oldPrices = ticks
          .flatMap {
              self.fetch(in: .usd)
          }
          .map {
            return $0.reduce(into: [Coin: Double]()) { result, coinInfo in
                  let price = coinInfo.price / (1 + coinInfo.relativeDayChange/100)
                  result[coinInfo.coin] = price
              }
          }
          .debug("oldPrices")

        return Observable.combineLatest(oldPrices, trades) { prices, trades in
            return trades.flatMap { coinInfo in
                guard let oldPrice = prices[coinInfo.coin] else { return nil }
                let relativeChange = 100 * (coinInfo.price - oldPrice) / oldPrice
                return CoinInfo(
                  date: coinInfo.date,
                  coin: coinInfo.coin,
                  price: coinInfo.price,
                  marketCap: coinInfo.marketCap,
                  currency: coinInfo.currency,
                  nominalDayChange: coinInfo.nominalDayChange,
                  relativeDayChange: relativeChange
                )
            }
        }
    }

    func percentToNominal(for price: Double, change: Double) -> Double {
        let pastPrice = price / (1 + (change/100))
        return price - pastPrice
    }

    func tradesResponseToCoinInfos(response: [Any]) -> [CoinInfo] {
        guard let item = response.first,
              let data = try? JSONSerialization.data(withJSONObject: item, options: []),
              let converted = try? JSONDecoder().decode(TradeResponse.self, from: data)
        else { return [] }
        return [CoinInfo(converted)]
    }

}
