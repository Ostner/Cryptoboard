//
//  CoinInfos+.swift
//  CoinCapPlatform
//
//  Created by Tobias Ostner on 24.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Domain

extension CoinInfo {

    init(_ tradesResponse: TradeResponse) {
        self.date =  Date()
        self.coin = Coin(name: tradesResponse.coinName, abbreviation: tradesResponse.coinAbbrev)
        self.price = tradesResponse.price
        self.marketCap = tradesResponse.marketCap
        self.currency = .usd
        self.nominalDayChange = CoinInfo.percentToNominal(
          for: tradesResponse.price,
          change: tradesResponse.relativeChange)
        self.relativeDayChange = tradesResponse.relativeChange
    }

    init(_ frontResponse: FrontResponseElement) {
        self.date = Date()
        self.coin = Coin(name: frontResponse.coinName, abbreviation: frontResponse.coinAbbrev)
        self.price = frontResponse.price
        self.marketCap = frontResponse.marketCap
        self.currency = .usd
        self.nominalDayChange = CoinInfo.percentToNominal(
          for: frontResponse.price,
          change: frontResponse.relativeChange)
        self.relativeDayChange = frontResponse.relativeChange
    }

    private static func percentToNominal(for price: Double, change: Double) -> Double {
        let pastPrice = price / (1 + (change/100))
        return price - pastPrice
    }

}
