//
//  CoinInfosUseCaseSpec.swift
//  CoinCapPlatformTests
//
//  Created by Tobias Ostner on 21.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Quick
import Nimble
import RxSwift
import RxBlocking
import RxTest
import Domain
@testable import CoinCapPlatform

class CoinInfosUseCaseSpec: QuickSpec {
    override func spec() {

        var subject: CoinCapPlatform.CoinInfosUseCase!

        beforeEach {
            subject = CoinInfosUseCase()
        }

        describe("fetch with stub data") {

            it("returns 2 coin info elements") {
                let subject = CoinInfosUseCase()
                let result = subject.fetch(in: .usd)
                  .toBlocking()
                  .materialize()
                switch result {
                case .completed(let elements):
                    expect(elements.first!.count) == 2
                case .failed:
                    fail("fetching stub data should not fail")
                }
            }
        }

        describe("percentToNominal") {

            context("positiv change") {

                let percentChange = 10.0
                let currentPrice = 110.0
                let expected = 10.0

                it("converts to nominal change") {
                    let nominal = subject.percentToNominal(for: currentPrice, change: percentChange)
                    expect(nominal) ≈ expected
                }
            }

            context("negative change") {

                let percentChange = -10.0
                let currentPrice = 90.0
                let expected = -10.0

                it("converts to nominal change") {
                    let nominal = subject.percentToNominal(for: currentPrice, change: percentChange)
                    expect(nominal) ≈ expected
                }
            }

        }
    }
}
