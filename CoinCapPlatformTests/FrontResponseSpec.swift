//
//  FrontResponseSpec.swift
//  CoinCapPlatformTests
//
//  Created by Tobias Ostner on 21.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Quick
import Nimble
@testable import CoinCapPlatform

class FrontResponseSpec: QuickSpec {
    override func spec() {

        describe("initialization with example") {

            var subject: [FrontResponseElement]!
            let coinName = "Bitcoin"
            let coinAbbrev = "BTC"
            let price = 3934.85
            let marketCap = 65173805891.25
            let relativeChange = -6.05

            beforeEach {
                let example = self.makeExample(
                  name: coinName,
                  abbreviation: coinAbbrev,
                  price: price,
                  marketCap: marketCap,
                  relativeChange: relativeChange
                )
                let decoder = JSONDecoder()
                subject = try! decoder.decode([FrontResponseElement].self, from: example)
            }

            it("decodes one element") {
                expect(subject.count) == 1
            }

            describe("element") {

                it("contains the coin name") {
                    expect(subject.first!.coinName) == coinName
                }

                it("contains the coin abbreviation") {
                    expect(subject.first!.coinAbbrev) == coinAbbrev
                }

                it("contains the price") {
                    expect(subject.first!.price) == price
                }

                it("contains the market capitalization") {
                    expect(subject.first!.marketCap) == marketCap
                }

                it("contains the 24 hour percent change") {
                    expect(subject.first!.relativeChange) == relativeChange
                }
            }

        }
    }

    private func makeExample(name: String,
                             abbreviation: String,
                             price: Double,
                             marketCap: Double,
                             relativeChange: Double)
      -> Data
    {
        return
          """
          [
            {
              "cap24hrChange": -6.05,
              "long": "\(name)",
              "mktcap": \(marketCap),
              "perc": \(relativeChange),
              "price": \(price),
              "shapeshift": true,
              "short": "\(abbreviation)",
              "supply": 16563225,
              "usdVolume": 2337600000,
              "volume": 2337600000,
              "vwapData": 3997.5639538606733,
              "vwapDataBTC": 3997.5639538606733
            }
          ]
          """.data(using: .utf8)!
    }
}
