//
//  TradeResponseSpec.swift
//  CoinCapPlatformTests
//
//  Created by Tobias Ostner on 22.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import CoinCapPlatform

final class TradeResponseSpec: QuickSpec {
    override func spec() {


        describe("Initialization with example") {

            var subject: TradeResponse!
            let exchange = "bittrex"
            let relativeChange = -6.96
            let price = 113.1814249336066
            let coinName = "Monero"
            let coinAbbrev = "XMR"
            let marketCap = 1581966162.998832

            beforeEach {
                let example = self.makeExample(
                  exchange: exchange,
                  relativeChange: relativeChange,
                  price: price,
                  coinName: coinName,
                  coinAbbrev: coinAbbrev,
                  marketCap: marketCap)
                subject = try! JSONDecoder().decode(TradeResponse.self, from: example)
            }

            it("has an exchange name") {
                expect(subject.exchange) == exchange
            }

            it("has a 24 hour percentage change") {
                expect(subject.relativeChange) == relativeChange
            }

            it("has a price") {
                expect(subject.price) ≈ price
            }

            it("has a coin name") {
                expect(subject.coinName) == coinName
            }

            it("has an abbreviation") {
                expect(subject.coinAbbrev) == coinAbbrev
            }

            it("has a market capitalization") {
                expect(subject.marketCap) ≈ marketCap
            }

        }
    }

    private func makeExample(exchange: String,
                             relativeChange: Double,
                             price: Double,
                             coinName: String,
                             coinAbbrev: String,
                             marketCap: Double)
      -> Data
    {
        let example =
          """
          {
            "coin": "XMR",
            "exchange_id": "\(exchange)",
            "market_id": "ETH_XMR",
            "message":
            {
              "coin": "XMR",
                "msg":
                {
                  "cap24hrChange": -6.96,
                  "long": "Monero",
                  "mktcap": 1581966162.998832,
                  "perc": -6.96,
                  "price": 113.1814249336066,
                  "shapeshift": true,
                  "short": "XMR",
                  "supply": 1574975,
                  "usdVolume": 4637300,
                  "volume": 4637300,
                  "vwapData": 109.5271673404815,
                  "vwapDataBTC": 109.5271673604815
                }
            },
            "msg":
              {
                "cap24hrChange": -6.96,
                "long": "\(coinName)",
                "mktcap": \(marketCap),
                "perc": \(relativeChange),
                "price": \(price),
                "shapeshift": true,
                "short": "\(coinAbbrev)",
                "supply": 1574975,
                "usdVolume": 4637300,
                "volume": 4637300,
                "vwapData": 109.5271673404815,
                "vwapDataBTC": 109.5271673604815
              }
          }
          """.data(using: .utf8)!
        return example
    }
}
