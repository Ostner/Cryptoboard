//
//  PriceInfo.swift
//  Domain
//
//  Created by Tobias Ostner on 11.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation

public struct PriceInfo {
    public let date: Date
    public let close: Double
    public let open: Double
    public let high: Double
    public let low: Double
}
