//
//  UseCaseProvider.swift
//  Domain
//
//  Created by Tobias Ostner on 12.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

public protocol UseCaseProvider {

    var coinInfosUseCase: CoinInfosUseCase { get }

    var coinPricesUseCase: CoinPricesUseCase { get }
}
