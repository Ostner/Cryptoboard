//
//  Coin.swift
//  Domain
//
//  Created by Tobias Ostner on 11.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

public struct Coin {
    public let name: String
    public let abbreviation: String

    public init(name: String, abbreviation: String) {
        self.name = name
        self.abbreviation = abbreviation
    }
}

extension Coin: Equatable {
    public static func ==(lhs: Coin, rhs: Coin) -> Bool {
        return lhs.name == rhs.name && lhs.abbreviation == rhs.abbreviation
    }
}

extension Coin: Hashable {
    public var hashValue: Int {
        return name.hashValue ^ abbreviation.hashValue
    }
}
