//
//  CoinPrices.swift
//  Domain
//
//  Created by Tobias Ostner on 11.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation

public struct CoinPrices {
    public let coin: Coin
    public let prices: [PriceInfo]

    public init(coin: Coin, prices: [PriceInfo]) {
        self.coin = coin
        self.prices = prices
    }
}
