//
//  Currency.swift
//  Domain
//
//  Created by Tobias Ostner on 11.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

public enum Currency: String {
    case usd = "USD"
}
