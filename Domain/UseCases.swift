//
//  CoinUseCase.swift
//  Domain
//
//  Created by Tobias Ostner on 11.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import RxSwift

public protocol CoinInfosUseCase {
    func fetch(coins: [Coin], in currency: Currency) -> Observable<[CoinInfo]>

    func fetch(in currency: Currency) -> Observable<[CoinInfo]>

    func connect(in currency: Currency) -> Observable<[CoinInfo]>
}

public protocol CoinPricesUseCase {
    func fetchDays(coin: Coin, in currency: Currency) -> Observable<CoinPrices>
}

