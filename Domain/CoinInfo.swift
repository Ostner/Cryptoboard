//
//  CoinInfo.swift
//  Domain
//
//  Created by Tobias Ostner on 11.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation

public struct CoinInfo {
    public let date: Date
    public let coin: Coin
    public let price: Double
    public let marketCap: Double
    public let currency: Currency
    public let nominalDayChange: Double
    public let relativeDayChange: Double

    public init(date: Date,
                coin: Coin,
                price: Double,
                marketCap: Double,
                currency: Currency,
                nominalDayChange: Double,
                relativeDayChange: Double) {
        self.date = date
        self.coin = coin
        self.price = price
        self.marketCap = marketCap
        self.currency = currency
        self.nominalDayChange = nominalDayChange
        self.relativeDayChange = relativeDayChange
    }
}

extension CoinInfo: Equatable {
    public static func ==(lhs: CoinInfo, rhs: CoinInfo) -> Bool {
        return
          lhs.date == rhs.date &&
          lhs.coin == rhs.coin &&
          lhs.price == rhs.price &&
          lhs.marketCap == rhs.marketCap &&
          lhs.currency == rhs.currency
    }
}

extension CoinInfo: Hashable {
    public var hashValue: Int {
        return
          date.timeIntervalSince1970.hashValue ^
          coin.hashValue ^
          price.hashValue ^
          marketCap.hashValue ^
          currency.hashValue ^
          nominalDayChange.hashValue ^
          relativeDayChange.hashValue
    }
}
