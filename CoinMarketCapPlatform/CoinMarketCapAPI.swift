//
//  CoinMarketCapAPI.swift
//  CoinMarketCapPlatform
//
//  Created by Tobias Ostner on 16.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation
import Moya

enum CoinMarketCapAPI {
    case ticker(currency: String, offset: Int, limit: Int)
}

extension CoinMarketCapAPI: TargetType {

    var baseURL: URL {
        return URL(string: "https://api.coinmarketcap.com/v1")!
    }

    var path: String {
        return "/ticker"
    }

    var method: Moya.Method {
        return .get
    }

    var task: Task {
        switch self {
        case let .ticker(currency, offset, limit):
            let params = ["start": "\(offset)", "limit": "\(limit)", "convert": currency]
            return .requestParameters(parameters: params, encoding: URLEncoding.queryString)
        }
    }

    var headers: [String: String]? {
        return nil
    }

    var sampleData: Data {
        return
          """
          [
            {"id": "bitcoin",
             "name": "Bitcoin",
             "symbol": "BTC",
             "rank": "1",
             "price_usd": "573.137",
             "price_btc": "1.0",
             "24h_volume_usd": "72855700.0",
             "market_cap_usd": "9080883500.0",
             "available_supply": "15844176.0",
             "total_supply": "15844176.0",
             "percent_change_1h": "0.04",
             "percent_change_24h": "-0.3",
             "percent_change_7d": "-0.57",
             "last_updated": "1472762067"}
          ]
          """.data(using: .utf8)!
    }
}
