//
//  CoinMarketCapPlatform.h
//  CoinMarketCapPlatform
//
//  Created by Tobias Ostner on 16.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CoinMarketCapPlatform.
FOUNDATION_EXPORT double CoinMarketCapPlatformVersionNumber;

//! Project version string for CoinMarketCapPlatform.
FOUNDATION_EXPORT const unsigned char CoinMarketCapPlatformVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoinMarketCapPlatform/PublicHeader.h>


