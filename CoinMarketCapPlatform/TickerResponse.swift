//
//  TickerResponse.swift
//  CoinMarketCapPlatform
//
//  Created by Tobias Ostner on 16.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain

struct TickerResponseElement: Decodable {

    let date: Date
    let coinName: String
    let coinAbbrev: String
    let priceUSD: Double
    let marketCapUSD: Double
    let relativeDayChange: Double

    enum CodingKeys: String, CodingKey {
        case date = "last_updated"
        case coinName = "name"
        case coinAbbrev = "symbol"
        case priceUSD = "price_usd"
        case marketCapUSD = "market_cap_usd"
        case relativeDayChange = "percent_change_24h"
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        guard let time = Double(try container.decode(String.self, forKey: .date)) else {
            throw DecodingError.dataCorruptedError(
              forKey: .date,
              in: container,
              debugDescription: "string is not convertible to Double")
        }
        self.date = Date(timeIntervalSince1970: time)
        self.coinName = try container.decode(String.self, forKey: .coinName)
        self.coinAbbrev = try container.decode(String.self, forKey: .coinAbbrev)
        guard let priceUSD = Double(try container.decode(String.self, forKey: .priceUSD)) else {
            throw DecodingError.dataCorruptedError(
              forKey: .priceUSD,
              in: container,
              debugDescription: "string is not convertible to Double")
        }
        self.priceUSD = priceUSD
        guard let marketCapUSD = Double(try container.decode(String.self, forKey: .marketCapUSD)) else {
            throw DecodingError.dataCorruptedError(
              forKey: .marketCapUSD,
              in: container,
              debugDescription: "string is not convertible to Double")
        }
        self.marketCapUSD = marketCapUSD
        guard let relativeDayChange = Double(try container.decode(String.self, forKey: .relativeDayChange)) else {
            throw DecodingError.dataCorruptedError(
              forKey: .relativeDayChange,
              in: container,
              debugDescription: "string is not convertible to Double")
        }
        self.relativeDayChange = relativeDayChange
    }

}
