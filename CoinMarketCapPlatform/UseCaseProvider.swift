//
//  UseCaseProvider.swift
//  CoinMarketCapPlatform
//
//  Created by Tobias Ostner on 18.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain
import Moya

public final class UseCaseProvider: Domain.UseCaseProvider {

    public init() {}

    public var coinInfosUseCase: Domain.CoinInfosUseCase {
        return CoinInfosUseCase()
    }

    public var coinPricesUseCase: Domain.CoinPricesUseCase {
        fatalError("not implemented yet")
    }
}
