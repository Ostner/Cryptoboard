//
//  CoinInfosUseCase.swift
//  CoinMarketCapPlatform
//
//  Created by Tobias Ostner on 18.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain
import RxSwift
import Moya

final class CoinInfosUseCase: Domain.CoinInfosUseCase {

    let provider: MoyaProvider<CoinMarketCapAPI>

    init() {
        #if TEST
        provider = MoyaProvider<CoinMarketCapAPI>(stubClosure: MoyaProvider.immediatelyStub)
        #else
        provider = MoyaProvider<CoinMarketCapAPI>(plugins: [NetworkLoggerPlugin()])
        #endif
    }

    func fetch(coins: [Coin], in currency: Currency) -> Observable<[CoinInfo]> {
        fatalError("no API for current information for specific coins")
    }

    func fetch(in currency: Currency) -> Observable<[CoinInfo]> {
        return provider
          .rx.request(.ticker(currency: currency.rawValue, offset: 0, limit: 50))
          .filterSuccessfulStatusCodes()
          .map([TickerResponseElement].self)
          .map { responses in
              return responses.map { elem in
                  return CoinInfo(
                    date: elem.date,
                    coin: Coin(name: elem.coinName, abbreviation: elem.coinAbbrev),
                    price: elem.priceUSD,
                    marketCap: elem.marketCapUSD,
                    currency: currency,
                    nominalDayChange: self.percentToNominal(for: elem.priceUSD, change: elem.relativeDayChange),
                    relativeDayChange: elem.relativeDayChange)
              }
          }
          .debug()
          .asObservable()
    }

    func connect(in currency: Currency) -> Observable<[CoinInfo]> {
        fatalError("not implemented yet")
    }

    func percentToNominal(for price: Double, change: Double) -> Double {
        let pastPrice = price / (1 + (change/100))
        return price - pastPrice
    }
}
