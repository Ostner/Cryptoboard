//
//  CoinInfosUseCaseSpec.swift
//  CoinMarketCapPlatformTests
//
//  Created by Tobias Ostner on 18.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Quick
import Nimble
import RxSwift
import RxBlocking
import RxTest
import Moya
import Domain
@testable import CoinMarketCapPlatform

final class CoinInfosUseCaseSpec: QuickSpec {
    override func spec() {

        var subject: CoinMarketCapPlatform.CoinInfosUseCase!

        beforeEach {
            subject = CoinInfosUseCase()
        }

        describe("fetch with stub data") {

            it("returns 1 coin info element") {
                subject = CoinInfosUseCase()
                let result = subject.fetch(in: .usd)
                  .toBlocking()
                  .materialize()
                switch result {
                case .completed(let elements):
                    expect(elements.first!.count) == 1
                case .failed:
                    fail("fetching stub data should not fail")
                }
            }
        }

        describe("percentToNominal") {

            context("positiv change") {

                let percentChange = 10.0
                let currentPrice = 110.0
                let expected = 10.0

                it("converts to nominal change") {
                    let nominal = subject.percentToNominal(for: currentPrice, change: percentChange)
                    expect(nominal) ≈ expected
                }
            }

            context("negative change") {

                let percentChange = -10.0
                let currentPrice = 90.0
                let expected = -10.0

                it("converts to nominal change") {
                    let nominal = subject.percentToNominal(for: currentPrice, change: percentChange)
                    expect(nominal) ≈ expected
                }
            }

        }

    }
}
