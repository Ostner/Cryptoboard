//
//  TickerResponse.swift
//  CoinMarketCapPlatformTests
//
//  Created by Tobias Ostner on 16.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Quick
import Nimble
@testable import CoinMarketCapPlatform


class TickerResponseSpec: QuickSpec {
    override func spec() {

        describe("initialization with example data") {

            var subject: [TickerResponseElement]!
            let coinName = "Bitcoin"
            let coinAbbrev = "BTC"
            let time = "1472762067"
            let priceUSD = "573.137"
            let marketCapUSD = "9080883500.0"
            let change =  "-0.3"

            beforeEach {
                let example = self.makeExample(
                  name: coinName,
                  abbreviation: coinAbbrev,
                  time: time,
                  priceUSD: priceUSD,
                  marketCapUSD: marketCapUSD,
                  change: change)
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                subject = try! decoder.decode([TickerResponseElement].self, from: example)
            }

            it("decodes one element") {
                expect(subject.count) == 1
            }

            describe("element") {

                it("contains the coin name") {
                    expect(subject.first!.coinName) == coinName
                }

                it("contains the coin abbreviation") {
                    expect(subject.first!.coinAbbrev) == "BTC"
                }

                it("contains the update time") {
                    let date = Date(timeIntervalSince1970: Double(time)!)
                    expect(subject.first!.date) == date
                }

                it("contains the USD price") {
                    let price = Double(priceUSD)!
                    expect(subject.first!.priceUSD) == price
                }

                it("contains the USD market capitalization") {
                    let cap = Double(marketCapUSD)!
                    expect(subject.first!.marketCapUSD) == cap
                }

                it("contains the 24h percent change") {
                    let percent = Double(change)!
                    expect(subject.first!.relativeDayChange) == percent
                }

            }

        }
    }

    private func makeExample(name: String,
                             abbreviation: String,
                             time: String,
                             priceUSD: String,
                             marketCapUSD: String,
                             change: String)
      -> Data
    {
        let example =
          """
          [
          {"id": "bitcoin",
          "name": "\(name)",
          "symbol": "\(abbreviation)",
          "rank": "1",
          "price_usd": "\(priceUSD)",
          "price_btc": "1.0",
          "24h_volume_usd": "72855700.0",
          "market_cap_usd": "\(marketCapUSD)",
          "available_supply": "15844176.0",
          "total_supply": "15844176.0",
          "percent_change_1h": "0.04",
          "percent_change_24h": "\(change)",
          "percent_change_7d": "-0.57",
          "last_updated": "\(time)"}
          ]
          """.data(using: .utf8)!
        return example
    }
}
