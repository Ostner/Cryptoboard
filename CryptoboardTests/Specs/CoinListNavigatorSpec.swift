//
//  CoinListNavigatorSpec.swift
//  CryptoboardTests
//
//  Created by Tobias Ostner on 17.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit
import Domain
import Quick
import Nimble
@testable import Cryptoboard

final class CoinListNavigatorSpec: QuickSpec {

    override func spec() {

        var navigator: CoinListNavigator!
        var navigationController: UINavigationController!

        beforeEach {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            navigationController = UINavigationController()
            let services = UseCaseProviderMock()
            navigator = CoinListNavigator(
              services: services,
              navigationController: navigationController,
              storyboard: storyboard)
        }

        describe("toCoinList()") {

            it("pushes a coin list view controller onto its navigation controller") {
                navigator.toCoinList()
                expect(navigationController.topViewController).to(beAnInstanceOf(CoinListViewController.self))
            }

            it("assigns a view model to the pushed view controller") {
                navigator.toCoinList()
                let coinListViewController = navigationController.topViewController as! CoinListViewController
                let viewModel = coinListViewController.viewModel
                expect(viewModel).notTo(beNil())
            }
        }

        describe("toCoinDetail()") {

            it("pushes a coin detail view controller onto its navigation controller") {
                navigator.toCoinDetail(coinInfo: CoinInfo.empty)
                expect(navigationController.topViewController).to(beAnInstanceOf(CoinDetailViewController.self))
            }

            it("assigns a view model to the pushed view controller") {
                navigator.toCoinDetail(coinInfo: CoinInfo.empty)
                let vc = navigationController.topViewController as! CoinDetailViewController
                let viewModel = vc.viewModel
                expect(viewModel).notTo(beNil())
            }
        }
    }
}


