//
//  CoinListItemViewModelSpec.swift
//  CryptoboardTests
//
//  Created by Tobias Ostner on 14.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Quick
import Nimble
import Domain
import RxTest
import RxSwift
import RxCocoa
import RxBlocking
@testable import Cryptoboard

class CoinListItemViewModelSpec: QuickSpec {


    override func spec() {

        let currency: Currency = .usd

        describe("output") {

            describe("coinName") {

                let coinName = "TestCoinName"

                it("is an observable for coin name") {
                    let coinInfo = CoinInfo.make(coinName: coinName)
                    let viewModel = CoinListItemViewModel(coinInfo: coinInfo)
                    let result = try! viewModel.transform(input: ())
                      .coinName
                      .toBlocking()
                      .single()
                    expect(result) == "TestCoinName"
                }
            }

            describe("relativeChange") {

                let relativeDayChange = 0.4


                var percentFormatter: NumberFormatter {
                    let formatter = NumberFormatter()
                    formatter.numberStyle = .percent
                    formatter.positivePrefix = "+"
                    formatter.multiplier = 1
                    formatter.maximumFractionDigits = 2
                    formatter.minimumFractionDigits = 2
                    return formatter
                }

                it("is an observable for relativeDayChange") {
                    let coinInfo = CoinInfo.make(relativeDayChange: relativeDayChange)
                    let viewModel = CoinListItemViewModel(coinInfo: coinInfo)
                    let result = try! viewModel.transform(input: ())
                      .relativeChange
                      .toBlocking()
                      .single()
                    expect(result) == percentFormatter.string(for: relativeDayChange)
                }
            }

            describe("isPositive") {

                it("is an observable for the direction of nominalDayChange") {
                    let coinInfo = CoinInfo.make(nominalDayChange: 1)
                    let viewModel = CoinListItemViewModel(coinInfo: coinInfo)
                    let result = try! viewModel.transform(input: ())
                      .isPositive
                      .toBlocking()
                      .single()
                    expect(result) == true
                }
            }

            describe("price") {

                let formatter: NumberFormatter = {
                    let formatter = NumberFormatter()
                    formatter.numberStyle = .currency
                    formatter.currencyCode = currency.rawValue
                    return formatter
                }()

                context("positive") {

                    context("greater than 1000") {

                        let price = 1_000_000.0

                        it("formats price without fractional digits") {
                            let coinInfo = CoinInfo.make(price: price)
                            let viewModel = CoinListItemViewModel(coinInfo: coinInfo)
                            let result = try! viewModel.transform(input: ())
                              .price
                              .toBlocking()
                              .single()
                            formatter.maximumFractionDigits = 0
                            formatter.minimumFractionDigits = 0
                            expect(result) == formatter.string(for: price)
                        }
                    }

                    context("between 1...1000") {

                        let price = 500.0

                        it("formats price with 2 fractional digits") {
                            let coinInfo = CoinInfo.make(price: price)
                            let viewModel = CoinListItemViewModel(coinInfo: coinInfo)
                            let result = try! viewModel.transform(input: ())
                              .price
                              .toBlocking()
                              .single()
                            formatter.maximumFractionDigits = 2
                            formatter.minimumFractionDigits = 2
                            expect(result) == formatter.string(for: price)
                        }
                    }

                    context("below 1") {

                        let price = 0.5

                        it("formats price with 4 fractional digits") {
                            let coinInfo = CoinInfo.make(price: price)
                            let viewModel = CoinListItemViewModel(coinInfo: coinInfo)
                            let result = try! viewModel.transform(input: ())
                              .price
                              .toBlocking()
                              .single()
                            formatter.maximumFractionDigits = 4
                            formatter.minimumFractionDigits = 4
                            expect(result) == formatter.string(for: price)
                        }
                    }

                }
            }

            describe("nominalChange") {

                let formatter: NumberFormatter = {
                    let formatter = NumberFormatter()
                    formatter.numberStyle = .currency
                    formatter.currencyCode = currency.rawValue
                    return formatter
                }()

                context("positive") {

                    context("above 1000") {

                        let nominalChange = 1_000_000.0

                        it("formats nominalDayChange without frational digits and with plus sign") {
                            let coinInfo = CoinInfo.make(nominalDayChange: nominalChange)
                            let viewModel = CoinListItemViewModel(coinInfo: coinInfo)
                            let result = try! viewModel.transform(input: ())
                              .nominalChange
                              .toBlocking()
                              .single()
                            formatter.maximumFractionDigits = 0
                            formatter.minimumFractionDigits = 0
                            let nominalChangeString: String? = "+" + formatter.string(for: nominalChange)!
                            expect(result) == nominalChangeString
                        }
                    }

                    context("between 1...1000") {

                        let nominalChange = 500.0

                        it("formats nominalDayChange with 2 frational digits and plus sign") {
                            let coinInfo = CoinInfo.make(nominalDayChange: nominalChange)
                            let viewModel = CoinListItemViewModel(coinInfo: coinInfo)
                            let result = try! viewModel.transform(input: ())
                              .nominalChange
                              .toBlocking()
                              .single()
                            formatter.maximumFractionDigits = 2
                            formatter.minimumFractionDigits = 2
                            let nominalChangeString: String? = "+" + formatter.string(for: nominalChange)!
                            expect(result) == nominalChangeString
                        }
                    }

                    describe("below 1") {

                        let nominalChange = 0.5

                        it("formats nominal day change with 4 frational digits and plus sign") {
                            let coinInfo = CoinInfo.make(nominalDayChange: nominalChange)
                            let viewModel = CoinListItemViewModel(coinInfo: coinInfo)
                            let result = try! viewModel.transform(input: ())
                              .nominalChange
                              .toBlocking()
                              .single()
                            formatter.maximumFractionDigits = 4
                            formatter.minimumFractionDigits = 4
                            let nominalChangeString: String? = "+" + formatter.string(for: nominalChange)!
                            expect(result) == nominalChangeString
                        }
                    }
                }
            }

        }
    }
}
