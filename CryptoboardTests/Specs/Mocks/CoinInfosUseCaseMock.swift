//
//  CoinInfosUseCaseMock.swift
//  CryptoboardTests
//
//  Created by Tobias Ostner on 17.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain
import RxSwift
import RxCocoa

final class CoinInfosUseCaseMock: Domain.CoinInfosUseCase {

    func fetch(coins: [Coin], in currency: Currency) -> Observable<[CoinInfo]> {
        return Observable.just([])
    }

    func fetch(in currency: Currency) -> Observable<[CoinInfo]> {
        return Observable.just([])
    }
    
    func connect(in currency: Currency) -> Observable<[CoinInfo]> {
        return Observable.just([])
    }
}
