//
//  CoinListNavigatorMock.swift
//  CryptoboardTests
//
//  Created by Tobias Ostner on 18.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import Cryptoboard
import Domain

final class CoinListNavigatorMock: CoinListNavigating {

    var hasCalledToCoinList = false
    var hasCalledToCoinDetail = false

    func toCoinList() {}

    func toCoinDetail(coinInfo: CoinInfo) {
        hasCalledToCoinDetail = true
    }
}

