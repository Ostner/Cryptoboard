//
//  UseCaseProviderMock.swift
//  CryptoboardTests
//
//  Created by Tobias Ostner on 17.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Domain

final class UseCaseProviderMock: Domain.UseCaseProvider {
    var coinInfosUseCase: CoinInfosUseCase {
        return CoinInfosUseCaseMock()
    }

    var coinPricesUseCase: CoinPricesUseCase {
        return CoinPricesUseCaseMock()
    }
}
