//
//  CoinPricesUseCaseMock.swift
//  CryptoboardTests
//
//  Created by Tobias Ostner on 15.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class CoinPricesUseCaseMock: Domain.CoinPricesUseCase {

    var hasCalledFetchDays = false

    func fetchDays(coin: Coin, in currency: Currency) -> Observable<CoinPrices> {
        hasCalledFetchDays = true
        return Observable.empty()
    }
}
