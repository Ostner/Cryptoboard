//
//  CoinListViewModelSpec.swift
//  CryptoboardTests
//
//  Created by Tobias Ostner on 17.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Quick
import Nimble
import Domain
import RxSwift
import RxCocoa
import RxTest
@testable import Cryptoboard

final class CoinListViewModelSpec: QuickSpec {

    override func spec() {

        var navigator: CoinListNavigatorMock!
        var output: CoinListViewModel.Output!

        beforeEach {
            navigator = CoinListNavigatorMock()
            let viewModel = CoinListViewModel(
              useCase: UseCaseProvider().coinInfosUseCase,
              navigator: navigator)
            let input = self.makeInput()
            output = viewModel.transform(input: input)
        }

        describe("output") {

            describe("coinInfos") {

                it("is an observable for coin info data") {
                    let result = try! output.coinInfos.toBlocking().single()
                    expect(result).to(beAnInstanceOf([CoinInfo].self))
                }
            }

            describe("selectedCoin") {

                it("is an observable for the selected coin info") {
                    let result = try! output.selectedCoin.toBlocking().single()
                    expect(result).to(beAnInstanceOf(CoinInfo.self))
                }

                it("calls toCoinDetail() on its navigator") {
                    _ = output.selectedCoin.toBlocking().materialize()
                    expect(navigator.hasCalledToCoinDetail).to(beTrue())
                }
            }
        }

    }

    private func makeInput() -> CoinListViewModel.Input {
        let trigger = Driver.just(())
        let indexPath = IndexPath(item: 0, section: 0)
        let selection = Driver.just(indexPath)
        return CoinListViewModel.Input(trigger: trigger, selection: selection)
    }
}
