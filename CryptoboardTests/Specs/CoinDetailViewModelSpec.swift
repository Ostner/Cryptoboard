//
//  CoinDetailViewModelSpec.swift
//  CryptoboardTests
//
//  Created by Tobias Ostner on 20.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Quick
import Nimble
import RxTest
import RxSwift
import RxCocoa
import Domain
import CryptoComparePlatform
@testable import Cryptoboard

final class CoinDetailViewModelSpec: QuickSpec {

    override func spec() {

        describe("output") {

            describe("coinName") {

                let coinName = "The Coin"

                it("is an observable for coinName") {
                    let viewModel = CoinDetailViewModel(
                      useCase: CryptoComparePlatform.UseCaseProvider().coinPricesUseCase,
                      coinInfo: CoinInfo.make(coinName: coinName))
                    let result = try! viewModel.transform(input: ())
                      .coinName
                      .toBlocking()
                      .single()
                    expect(result) == coinName
                }
            }

            describe("shortDescription") {

                context("non existing description") {

                    let coinName = "A coin that did not exist"

                    it("is an observable with a nil event") {
                        let viewModel = CoinDetailViewModel(
                          useCase: CryptoComparePlatform.UseCaseProvider().coinPricesUseCase,
                          coinInfo: CoinInfo.make(coinName: coinName))
                        let result = try! viewModel.transform(input: ())
                          .shortDescription
                          .toBlocking()
                          .single()
                        expect(result).to(beNil())
                    }

                }

                context("existing description") {

                    let coinName = "Bitcoin"

                    it("is an observable with a non empty string event") {
                        let viewModel = CoinDetailViewModel(
                          useCase: CryptoComparePlatform.UseCaseProvider().coinPricesUseCase,
                          coinInfo: CoinInfo.make(coinName: coinName))
                        let result = try! viewModel.transform(input: ())
                          .shortDescription
                          .toBlocking()
                          .single()
                        expect(result).notTo(beNil())
                    }
                }
            }

            describe("abbreviation") {

                let abbreviation = "123"

                it("is an observable for the coinInfo coin's abbreviation") {
                    let viewModel = CoinDetailViewModel(
                      useCase: CryptoComparePlatform.UseCaseProvider().coinPricesUseCase,
                      coinInfo: CoinInfo.make(abbreviation: abbreviation))
                    let result = try! viewModel.transform(input: ())
                      .abbreviation
                      .toBlocking()
                      .single()
                    expect(result) == abbreviation
                }
            }

            describe("prices") {

                let subject = UseCaseProviderMock().coinPricesUseCase as! CoinPricesUseCaseMock

                it("calls fetch on coin prices use case") {
                    expect(subject.hasCalledFetchDays) == false
                    let viewModel = CoinDetailViewModel(
                      useCase: subject,
                      coinInfo: CoinInfo.empty)
                    _ = viewModel.transform(input: ())
                      .prices
                      .toBlocking()
                    expect(subject.hasCalledFetchDays) == true
                }
            }
        }
    }
}
