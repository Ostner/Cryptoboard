//
//  CoinInfo+.swift
//  CryptoboardTests
//
//  Created by Tobias Ostner on 19.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Domain

extension CoinInfo {

    static var empty: CoinInfo {
        return CoinInfo(
          date: Date(),
          coin: Coin(name: "", abbreviation: ""),
          price: 0.0,
          marketCap: 0.0,
          currency: .usd,
          nominalDayChange: 0.0,
          relativeDayChange: 0.0)
    }

    static func make(coinName: String) -> CoinInfo {
        return CoinInfo(
          date: Date(),
          coin: Coin(name: coinName, abbreviation: ""),
          price: 0.0,
          marketCap: 0.0,
          currency: .usd,
          nominalDayChange: 0.0,
          relativeDayChange: 0.0)
    }

    static func make(abbreviation: String) -> CoinInfo {
        return CoinInfo(
          date: Date(),
          coin: Coin(name: "", abbreviation: abbreviation),
          price: 0.0,
          marketCap: 0.0,
          currency: .usd,
          nominalDayChange: 0.0,
          relativeDayChange: 0.0)
    }

    static func make(relativeDayChange: Double) -> CoinInfo {
        return CoinInfo(
          date: Date(),
          coin: Coin(name: "", abbreviation: ""),
          price: 0.0,
          marketCap: 0.0,
          currency: .usd,
          nominalDayChange: 0.0,
          relativeDayChange: relativeDayChange)
    }

    static func make(nominalDayChange: Double) -> CoinInfo {
        return CoinInfo(
          date: Date(),
          coin: Coin(name: "", abbreviation: ""),
          price: 0.0,
          marketCap: 0.0,
          currency: .usd,
          nominalDayChange: nominalDayChange,
          relativeDayChange: 0.0)
    }

    static func make(price: Double) -> CoinInfo {
        return CoinInfo(
          date: Date(),
          coin: Coin(name: "", abbreviation: ""),
          price: price,
          marketCap: 0.0,
          currency: .usd,
          nominalDayChange: 0.0,
          relativeDayChange: 0.0)
    }
}


