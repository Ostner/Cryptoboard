//
//  SegueToCoinDetail.swift
//  CryptoboardTests
//
//  Created by Tobias Ostner on 18.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import XCTest
import KIF
import Cryptoboard

final class CoinDetailFeature: KIFTestCase {

    override func afterEach() {
        tester().tapView(withAccessibilityLabel: "Cryptoboard")
    }

    func testSegueToCoinDetailFeature() {
        givenACollectionViewItemExist(withIdentifier: "Bitcoin")
        whenUserTapsItemInCoinList(withIdentifier: "Bitcoin")
        thenUserWillSeeTheCoinDetail()
    }

    func testCoinDetailContainsCoinName() {
        givenNavigationFromCryptoboardToCoinDetail(forCoin: "Bitcoin")
        thenTheNameIsVisible(forCoin: "Bitcoin")
    }

    func testCoinDetailContainsAShortDescription() {
        givenNavigationFromCryptoboardToCoinDetail(forCoin: "Bitcoin")
        thenAShortDescriptionIsVisible(forCoin: "Bitcoin")
    }

    func testCoinDetailContainsIcon() {
        givenNavigationFromCryptoboardToCoinDetail(forCoin: "Bitcoin")
        thenAnIconIsVisible()
    }

    func testCoinDetailContainsViewForGraph() {
        givenNavigationFromCryptoboardToCoinDetail(forCoin: "Bitcoin")
        thenAPriceGraphViewIsVisible()
    }

    // MARK: Given

    private func givenNavigationFromCryptoboardToCoinDetail(forCoin name: String) {
        tester().waitForView(withAccessibilityLabel: name)
        tester().tapView(withAccessibilityLabel: name)
    }

    private func givenACollectionViewItemExist(withIdentifier identifier: String) {
        tester().waitForView(withAccessibilityLabel: identifier)
    }

    // MARK: When

    private func whenUserTapsItemInCoinList(withIdentifier identifier: String) {
        tester().tapView(withAccessibilityLabel: identifier)
    }

    // MARK: Then

    private func thenUserWillSeeTheCoinDetail() {
        tester().waitForView(withAccessibilityLabel: "Coin Detail")
    }

    private func thenTheNameIsVisible(forCoin name: String) {
        let title = tester().waitForView(withAccessibilityLabel: "Bitcoin")
        tester().expect(title, toContainText: name)
    }

    private func thenAShortDescriptionIsVisible(forCoin name: String) {
        let description = shortDescription[name]
        let label = tester().waitForView(withAccessibilityLabel: description)
        tester().expect(label, toContainText: description)
    }

    private func thenAnIconIsVisible() {
        tester().waitForView(withAccessibilityLabel: "Coin Icon")
    }

    private func thenAPriceGraphViewIsVisible() {
        tester().waitForView(withAccessibilityLabel: "Price Graph")
    }

}
