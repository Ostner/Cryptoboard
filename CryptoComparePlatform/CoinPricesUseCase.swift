//
//  CoinPricesUseCase.swift
//  CryptoComparePlatform
//
//  Created by Tobias Ostner on 11.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain
import RxSwift
import Moya

final class CoinPricesUseCase: Domain.CoinPricesUseCase {

    let provider: MoyaProvider<CryptoCompareAPI>

    init(provider: MoyaProvider<CryptoCompareAPI>) {
        self.provider = provider
    }

    func fetchDays(coin: Coin, in currency: Currency) -> Observable<CoinPrices> {
        return provider
          .rx.request(.histoDay(coin: coin.abbreviation, currency: currency.rawValue))
          .debug()
          .filterSuccessfulStatusCodes()
          .map(PriceHistoryResponse.self)
          .map { CoinPrices(coin: coin, prices: $0.data) }
          .debug()
          .asObservable()
    }
}
