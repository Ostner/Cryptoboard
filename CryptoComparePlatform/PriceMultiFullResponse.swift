//
//  PriceMultiFullResponse.swift
//  CryptoComparePlatform
//
//  Created by Tobias Ostner on 11.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain

struct PriceMultiFullResponse: Decodable {

    enum FormatCodingKeys: String, CodingKey {
        case raw = "RAW"
        case display = "DISPLAY"
    }

    struct CoinsCodingKeys: CodingKey {
        var stringValue: String
        var intValue: Int? { return nil }

        init?(stringValue: String) {
            self.stringValue = stringValue
        }

        init?(intValue: Int) {
            return nil
        }
    }

    struct DataCodingKeys: CodingKey {
        var stringValue: String
        var intValue: Int? { return nil }

        init?(stringValue: String) {
            self.stringValue = stringValue
        }

        init?(intValue: Int) {
            return nil
        }

        static let price = DataCodingKeys(stringValue: "PRICE")!
        static let absoluteChange24hour = DataCodingKeys(stringValue: "CHANGE24HOUR")!
        static let percentChange24hour = DataCodingKeys(stringValue: "CHANGEPCT24HOUR")!
        static let date = DataCodingKeys(stringValue: "LASTUPDATE")!
        static let marketCap = DataCodingKeys(stringValue: "MKTCAP")!
    }

    let coinInfos: [CoinInfo]

    init(from decoder: Decoder) throws {
        var coinInfos: [CoinInfo] = []

        let formatContainer = try decoder.container(keyedBy: FormatCodingKeys.self)
        let coinContainers = try formatContainer.nestedContainer(
          keyedBy: CoinsCodingKeys.self,
          forKey: .raw)

        for coinAbbrev in coinContainers.allKeys {
            let currencyContainers = try coinContainers.nestedContainer(
              keyedBy: DataCodingKeys.self,
              forKey: coinAbbrev)

            guard let currencyAbbrev = currencyContainers.allKeys.first
            else { throw DecodingError.dataCorruptedError(
                     forKey: coinAbbrev,
                     in: coinContainers,
                     debugDescription: "Empty price infos")
            }

            let infos = try currencyContainers.nestedContainer(
              keyedBy: DataCodingKeys.self,
              forKey: currencyAbbrev)

            // TODO price is not always of type double but sometimes of type string
            let price: Double
            do {
                price = try infos.decode(Double.self, forKey: .price)
            } catch DecodingError.typeMismatch {
                price = Double(try infos.decode(String.self, forKey: .price))!
            }

            let nominalChange = try infos.decode(
              Double.self,
              forKey: .absoluteChange24hour)
            let relativeChange = try infos.decode(
              Double.self,
              forKey: .percentChange24hour)
            guard let currency = Currency(rawValue: currencyAbbrev.stringValue.uppercased())
            else { throw DecodingError.dataCorruptedError(
                     forKey: currencyAbbrev,
                     in: currencyContainers,
                     debugDescription: "Currency abbreviation is unknown")
            }
            guard let coinName = abbreviations[coinAbbrev.stringValue]
            else { throw DecodingError.dataCorruptedError(
                     forKey: coinAbbrev,
                     in: coinContainers,
                     debugDescription: "Coin abbreviation is unknown")
            }
            let date = try infos.decode(Date.self, forKey: .date)
            let marketCap = try infos.decode(Double.self, forKey: .marketCap)

            let coin = Coin(
              name: coinName,
              abbreviation: coinAbbrev.stringValue)
            let coinInfo = CoinInfo(
              date: date,
              coin: coin,
              price: price,
              marketCap: marketCap,
              currency: currency,
              nominalDayChange: nominalChange,
              relativeDayChange: relativeChange)
            coinInfos.append(coinInfo)
        }

        self.coinInfos = coinInfos
    }

    // TODO only temporary solution
    let abbreviations = ["BTC": "Bitcoin",
                         "ETH": "Ethereum",
                         "BCH": "Bitcoin Cash",
                         "XRP": "Ripple",
                         "LTC": "Litecoin",
                         "DASH": "Dash",
                         "NEO": "Neo",
                         "BCCOIN": "BitConnect",
                         "XEM": "Neo",
                         "XMR": "Monero",
                         "MIOTA": "IOTA",
                         "ETC": "Ethereum Classic",
                         "QTUM": "Qtum",
                         "ADA": "Cardano",
                         "OMG": "OmiseGO",
                         "ZEC": "Zcash",
                         "LSK": "Lisk",
                         "XLM": "Stellar Lumens",
                         "USDT": "Tether",
                         "EOS": "EOS",
                         ]
}
