//
//  UseCaseProvider.swift
//  CryptoComparePlatform
//
//  Created by Tobias Ostner on 12.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain
import Moya

public final class UseCaseProvider: Domain.UseCaseProvider {

    private let provider: MoyaProvider<CryptoCompareAPI>

    public init() {
        #if TEST
        provider = MoyaProvider<CryptoCompareAPI>(stubClosure: MoyaProvider.immediatelyStub)
        #else
        provider = MoyaProvider<CryptoCompareAPI>(plugins: [NetworkLoggerPlugin()])
        #endif
    }

    public var coinInfosUseCase: Domain.CoinInfosUseCase {
        return CoinInfosUseCase()
    }

    public var coinPricesUseCase: Domain.CoinPricesUseCase {
        return CoinPricesUseCase(provider: provider)
    }

}
