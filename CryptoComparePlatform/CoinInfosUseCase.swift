//
//  CoinInfosUseCase.swift
//  CryptoComparePlatform
//
//  Created by Tobias Ostner on 11.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import RxSwift
import Domain
import Moya

final class CoinInfosUseCase: Domain.CoinInfosUseCase {

    private var coinList: [String:String] {
        return [
          "BTC": "Bitcoin",
          "ETH": "Ethereum",
          "BCH": "Bitcoin Cash",
          "XRP": "Ripple",
          "LTC": "Litecoin",
          "DASH": "Dash",
          "NEO": "Neo",
          "XEM": "Neo",
          "XMR": "Monero",
          "MIOTA": "IOTA",
          "ETC": "Ethereum Classic",
          "QTUM": "Qtum",
          "ADA": "Cardano",
          "OMG": "OmiseGO",
          "ZEC": "Zcash",
          "LSK": "Lisk",
          "XLM": "Stellar Lumens",
          "USDT": "Tether",
          "EOS": "EOS",
        ]
    }


    let provider: MoyaProvider<CryptoCompareAPI>

    init() {
        #if TEST
        provider = MoyaProvider<CryptoCompareAPI>(stubClosure: MoyaProvider.immediatelyStub)
        #else
        provider = MoyaProvider<CryptoCompareAPI>(plugins: [NetworkLoggerPlugin()])
        #endif
    }

    func fetch(coins: [Coin], in currency: Currency) -> Observable<[CoinInfo]> {
        let coinAbbrevs = coins.map { $0.abbreviation }
        let currencyString = currency.rawValue
        return provider
            .rx.request(.priceMultiFull(coins: coinAbbrevs, currency: currencyString))
            .filterSuccessfulStatusCodes()
            .map(PriceMultiFullResponse.self)
            .map { $0.coinInfos }
            .debug()
            .asObservable()
    }

    func fetch(in currency: Currency) -> Observable<[CoinInfo]> {
        let coins = coinList.map { key, value in Coin(name: value, abbreviation: key) }
        return fetch(coins: coins, in: currency)
    }

    func connect(in currency: Currency) -> Observable<[CoinInfo]> {
        fatalError("not implemented yet")
    }
}
