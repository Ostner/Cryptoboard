//
//  CryptoCompareAPI.swift
//  CryptoComparePlatform
//
//  Created by Tobias Ostner on 11.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Foundation
import Moya

enum CryptoCompareAPI {
    case priceMultiFull(coins: [String], currency: String)
    case histoDay(coin: String, currency: String)
}

extension CryptoCompareAPI: TargetType {

    var baseURL: URL {
        return URL(string: "https://min-api.cryptocompare.com/data")!
    }

    var path: String {
        switch self {
        case .priceMultiFull: return "/pricemultifull"
        case .histoDay: return "/histoday"
        }
    }

    var method: Moya.Method {
        return .get
    }

    var task: Task {
        switch self {
        case let .priceMultiFull(coins, currency):
            let concatedCoins = coins
                .map { $0.uppercased() }
                .joined(separator: ",")
            let parameters = ["fsyms": concatedCoins, "tsyms": currency]
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        case let .histoDay(coin, currency):
            let parameters = ["fsym": coin, "tsym": currency, "limit": "30"]
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        }
    }

    var headers: [String: String]? {
        return nil
    }

    var sampleData: Data {
        switch self {
        case .priceMultiFull:
            let jsonData =
            """
            {"RAW":
              {"BTC":
                {"USD":
                  {"TYPE":"5",
                   "MARKET":"CCCAGG",
                   "FROMSYMBOL":"BTC",
                   "TOSYMBOL":"USD",
                   "FLAGS":"4",
                   "PRICE":16784.37,
                   "LASTUPDATE":1513073489,
                   "LASTVOLUME":0.00216256,
                   "LASTVOLUMETO":36.544668928,
                   "LASTTRADEID":"1513073489.2927",
                   "VOLUMEDAY":56080.46441963462,
                   "VOLUMEDAYTO":933273804.854428,
                   "VOLUME24HOUR":135680.07587984743,
                   "VOLUME24HOURTO":2265388767.219989,
                   "OPENDAY":16732.47,
                   "HIGHDAY":16977.98,
                   "LOWDAY":16254.53,
                   "OPEN24HOUR":16616.13,
                   "HIGH24HOUR":17434.79,
                   "LOW24HOUR":16015.28,
                   "LASTMARKET":"Kraken",
                   "CHANGE24HOUR":168.23999999999796,
                   "CHANGEPCT24HOUR":1.0125101332259554,
                   "CHANGEDAY":51.89999999999782,
                   "CHANGEPCTDAY":0.3101753656214403,
                   "SUPPLY":16735962,
                   "MKTCAP":280902578513.94,
                   "TOTALVOLUME24H":403339.00866678497,
                   "TOTALVOLUME24HTO":6757875328.92108}}}}
            """.data(using: .utf8)!
            return jsonData
        case .histoDay:
            let jsonData =
              """
              {"Response":"Success",
               "Type":100,
               "Aggregated":false,
               "Data":[
                 {"time":1515715200,
                  "close":13841.19,
                  "high":14129.08,
                  "low":12851.91,
                  "open":13308.06,
                  "volumefrom":102068.02,
                  "volumeto":1402292716.89},
                ],
                "TimeTo":1515801600,
                "TimeFrom":1515715200,
                "FirstValueInArray":true,
                "ConversionType":
                  {"type":"direct",
                   "conversionSymbol":""}}
              """.data(using: .utf8)!
            return jsonData
        }
    }
}

