//
//  PriceHistoryResponse.swift
//  CryptoComparePlatform
//
//  Created by Tobias Ostner on 11.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation
import Domain

extension PriceInfo: Decodable {
    enum CodingKeys: String, CodingKey {
        case date = "time"
        case close
        case open
        case high
        case low
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.date = try container.decode(Date.self, forKey: .date)
        self.close = try container.decode(Double.self, forKey: .close)
        self.open = try container.decode(Double.self, forKey: .open)
        self.high = try container.decode(Double.self, forKey: .high)
        self.low = try container.decode(Double.self, forKey: .low)
    }

}

struct PriceHistoryResponse: Decodable {

    enum CodingKeys: String, CodingKey {
        case data = "Data"
    }

    let data: [PriceInfo]
}
