//
//  CryptoComparePlatform.h
//  CryptoComparePlatform
//
//  Created by Tobias Ostner on 11.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CryptoComparePlatform.
FOUNDATION_EXPORT double CryptoComparePlatformVersionNumber;

//! Project version string for CryptoComparePlatform.
FOUNDATION_EXPORT const unsigned char CryptoComparePlatformVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CryptoComparePlatform/PublicHeader.h>


