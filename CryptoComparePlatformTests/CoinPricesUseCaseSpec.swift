//
//  CoinPricesUseCaseSpec.swift
//  CryptoComparePlatformTests
//
//  Created by Tobias Ostner on 13.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Quick
import Nimble
import Domain
@testable import CryptoComparePlatform

class CoinPricesUseCaseSpec: QuickSpec {
    override func spec() {

        describe("fetchDays with stub data") {

            var subject: Domain.CoinPricesUseCase!

            let testCoin = Coin(name: "test", abbreviation: "TE")

            beforeEach {
                let provider = UseCaseProvider()
                subject = provider.coinPricesUseCase
            }

            it("returns non empty coin prices event") {
                let result = subject.fetchDays(coin: testCoin, in: .usd)
                  .toBlocking()
                  .materialize()
                switch result {
                case .completed(let coinPrices):
                    expect(coinPrices.first!.prices).notTo(beEmpty())
                case .failed:
                    fail("fetching stub data for historic prices should not fail")
                }
            }

        }
    }
}
