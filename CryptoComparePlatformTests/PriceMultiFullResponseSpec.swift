//
//  PriceMultiFullResponseSpec.swift
//  CryptoComparePlatformTests
//
//  Created by Tobias Ostner on 12.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Quick
import Nimble
import Domain
import Moya
import RxSwift
import RxTest
@testable import CryptoComparePlatform

class PriceMultiFullResponseSpec: QuickSpec {
    override func spec() {

        describe("Initialization with example JSON data") {

            let example: Data = {
                let dict: [String: Any] =
                    ["RAW": ["BTC": ["USD":
                        [
                            "PRICE": 1,
                            "CHANGE24HOUR": 2,
                            "CHANGEPCT24HOUR": 3,
                            "LASTUPDATE": 4,
                            "MKTCAP": 5
                        ]]],
                     "DISPLAY": []]
                let jsonData = try! JSONSerialization.data(withJSONObject: dict, options: [])
                return jsonData
            }()

            it("throws no errors") {
                expect { try JSONDecoder().decode(PriceMultiFullResponse.self, from: example) }.toNot(throwError())
            }

            describe("when decoding") {

                var response: PriceMultiFullResponse!

                beforeEach {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .secondsSince1970
                    response = try! decoder.decode(PriceMultiFullResponse.self, from: example)
                }

                it("decodes one CoinInfo element") {
                    expect(response.coinInfos.count) == 1
                }

                it("decodes the example price") {
                    expect(response.coinInfos.first!.price) == 1
                }

                it("decodes the example absolute 24 hour change") {
                    expect(response.coinInfos.first!.nominalDayChange) == 2
                }

                it("decodes the example relative 24 hour change") {
                    expect(response.coinInfos.first!.relativeDayChange) == 3
                }

                it("decodes the example coin name abbreviation") {
                    expect(response.coinInfos.first!.coin.abbreviation) == "BTC"
                }

                it("maps the example coin name from the coin abbreviation") {
                    expect(response.coinInfos.first!.coin.name) == "Bitcoin"
                }

                it("decodes the example currency") {
                    expect(response.coinInfos.first!.currency) == Currency.usd
                }

                it("decodes the example date") {
                    let date = Date(timeIntervalSince1970: 4)
                    expect(response.coinInfos.first!.date) == date
                }

                it("decodes the example market capitalization") {
                    expect(response.coinInfos.first!.marketCap) == 5
                }
            }
        }
    }
}
