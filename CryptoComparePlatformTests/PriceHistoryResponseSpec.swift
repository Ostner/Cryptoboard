//
//  PriceHistoryResponseSpec.swift
//  CryptoComparePlatformTests
//
//  Created by Tobias Ostner on 11.01.18.
//  Copyright © 2018 Tobias Ostner. All rights reserved.
//

import Foundation
import Nimble
import Quick
import Domain
@testable import CryptoComparePlatform

class PriceHistoryResponseSpec: QuickSpec {
    override func spec() {

        describe("Decoding example JSON") {

            let time: TimeInterval = 1499990400
            let close = 1914.09
            let high = 2370.53
            let low = 1808.99
            let open = 2362.44

            var example: Data!

            beforeEach {
                example = self.example(
                  time: time,
                  open: open,
                  close: close,
                  high: high,
                  low: low)
            }

            it("does not throw an error") {
                expect { try! JSONDecoder().decode(PriceHistoryResponse.self, from: example) }.toNot(throwError())
            }

            describe("data") {

                var subject: [PriceInfo]!

                beforeEach {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .secondsSince1970
                    subject = try! decoder.decode(PriceHistoryResponse.self, from: example).data
                }

                it("decodes one element") {
                    expect(subject.count) == 1
                }

                it("decodes the opening price") {
                    expect(subject.first!.open) == open
                }

                it("decodes the closing price") {
                    expect(subject.first!.close) == close
                }

                it("decodes the price high") {
                    expect(subject.first!.high) == high
                }

                it("decodes the price low") {
                    expect(subject.first!.low) == low
                }

                it("decodes the date") {
                    let date = Date(timeIntervalSince1970: time)
                    expect(subject.first!.date) == date
                }

            }

        }
    }

    private func example(time: TimeInterval, open: Double, close: Double, high: Double, low: Double) -> Data {
        return
          """
          {"Response": "Success",
          "Type": 100,
          "Aggregated": false,
          "Data": [{"time": \(time),
          "close": \(close),
          "high": \(high),
          "low": \(low),
          "open": \(open)}]}
          """.data(using: .utf8)!
    }
}
