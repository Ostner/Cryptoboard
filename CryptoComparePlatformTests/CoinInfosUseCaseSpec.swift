//
//  CoinInfosUseCaseSpec.swift
//  CryptoComparePlatformTests
//
//  Created by Tobias Ostner on 12.12.17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import Quick
import Nimble
import RxSwift
import RxTest
import Moya
import Domain
import RxBlocking
@testable import CryptoComparePlatform

class CoinInfosUseCaseSpec: QuickSpec {
    override func spec() {

        var subject: Domain.CoinInfosUseCase!

        beforeEach {
            subject = CoinInfosUseCase()
        }

        describe("fetchCurrent with stub data") {

            it("returns one coin info element") {
                let result = subject.fetch(coins: [], in: .usd)
                  .toBlocking()
                  .materialize()
                switch result {
                case .completed(let elements):
                    expect(elements).notTo(beEmpty())
                case .failed:
                    fail("fetching stub data should not fail")
                }
            }
        }

        describe("fetch with with stub data") {

            it("returns one coin info element") {
                let result = subject.fetch(in: .usd)
                  .toBlocking()
                  .materialize()
                switch result {
                case .completed(let elements):
                    expect(elements).notTo(beEmpty())
                case .failed:
                    fail("fetching stub data should not fail")
                }
            }
        }

    }
}
